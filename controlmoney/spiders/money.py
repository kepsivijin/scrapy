# -*- coding: utf-8 -*-
import scrapy
from controlmoney.items import ControlmoneyItem


class MoneySpider(scrapy.Spider):
    name = 'money'
    allowed_domains = ['moneycontrol.com']
    start_urls = ['https://www.moneycontrol.com/stocks/marketinfo/netsales/bse/abrasives.html']

    def parse(self, response):
        item = ControlmoneyItem()
        a=response.css("table.table4  tr")
        item['heading']=a.css("td b").css("h2::text").extract()
        item['mainheading']=response.css("div.FL.gry10").css("::text")[-1:].extract()
        for tr in a[8:-5]:

            tds=tr.css('td')
            try:
                item['detail']=tds[0].css("::text").extract()
                item['march18'] = tds[1].css("::text").extract()
                item['march17'] = tds[2].css("::text").extract()
                item['march16'] = tds[3].css("::text").extract()
                item['march15'] = tds[4].css("::text").extract()
                item['march14']=tds[5].css("::text").extract()
                yield item
            except IndexError:
                pass
            continue
            

        for i in response.css("li a.opt_notselected::attr(href)")[90:].extract():
            next_page = response.urljoin(i)
            print next_page
            yield scrapy.Request(next_page, callback=self.parse)
            
        for i in response.css("table.tbldata14.bdrtpg td.brdrgtgry").css("a.bl_12::attr('href')").extract():
          next_page=response.urljoin(i)
          yield scrapy.Request(next_page, callback=self.parse)
        next=response.css("div.FL.leftNav").css("dl#slider dt").css("a::attr('href')")[6].extract()
        next_page1=response.urljoin(next)
        yield scrapy.Request(next_page1,callback=self.parse)
        for j in response.css("div.FL.leftNav").css("dl#slider dd").css("ul.act li").css("a::attr('href')")[0:2].extract():
            next_page2=response.urljoin(j)
            yield scrapy.Request(next_page2,callback=self.parse)
