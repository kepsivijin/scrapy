# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ControlmoneyItem(scrapy.Item):
    # define the fields for your item here like:
      heading = scrapy.Field()
      mainheading = scrapy.Field()
      detail = scrapy.Field()
      march18 = scrapy.Field()
      march17 = scrapy.Field()
      march16 = scrapy.Field()
      march15 = scrapy.Field()
      march14 = scrapy.Field()
